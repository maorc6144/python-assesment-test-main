# **DevOps Assesment**

Hi There! Welcome to the Cyberark Devops Assesment!

**The repository has a skeleton project with Flask, fell free to continue with it or change the implementation to something you feel more comfortable with**


## Stage 1: Python Service (**)
In this stage you'll create a python service based on Flask that analyse Users data and retrieve the Country of residence.\
We have a Users database ([Users DB](https://randomuser.me/)) which will be used in this stage.

**Implement the following Endpoints (GET):**
1.  route: "/users" \
    behavior: Should Retrieve Users in a Json Object from the stated API : "https://randomuser.me/api". \
    Response is a JSON array of objects just like the original api
    parameters: results<optional> - number of users to return (defaults to 1).   
    extra information: The "randomUser" api already support "results" as a query param (with the same logic as we requested)
    
        example: http://localhost/users → Will GET 1 User Data
        example: http://localhost/users?results=1000 → Will GET 1000 Users Data  

2. route: "/users/analyze" \
    behavior: Should Calculate the amount of times each country appears in the response. \
    parameters: results<optional> - number of users to return (defaults to 1). 

    example: http://localhost/users/analyze?result=1000 →  
            {"Canada": 64, "Netherlands": 61, "New Zealand": 45, "Turkey": 59} 

## Stage 2: Dockerfile (*)
In this stage you'll create a docker container from your python application and push it to ECR. \
The Application Dockerfile is provided in the github repo. We suspect there is a Bug there, but unfortuntly we didn't have the time to fix it... could you help us? \
Note that the project build is dependent on it...

    Build and run the docker app image \ 
    Make sure the Tag for the Docker is as follows: \
    **localhost:5001/flask-assesment:latest**
    
## Stage 3: Jenkinsfile (*)
We've created a Jenkinsfile in the repo that runs on a Linux Agent with a label "linux-node".
There is a Jenkins Service running on the Windows machine under url : "localhost:8080", you can login with user/pass "admin/admin"
    
    3.1 Please create a Jenkins Pipeline Job that will Clone the Repo from Github with the url: 
        https://github.com/cyberarkDevopsCandidate/python-assesment-test.git
        The Job should run the Jenkinsfile in that repo.
**      use credential: "cyberarkuser-temptoken"**

    3.2 Please fill in the stages in the Jenkinsfile as you see fit.
        The stages should include Python Project lifecycle that will include: 
        Dependecy installation - install requirements.txt with python pip, 
        Unittests - run tests with pytest, 
        Docker image build (From previous step (2)
        Docker Integration test (that will check if the application is alive) - 
        run the created Docker from Image and send a GET request to receive Status Code 200
    
    
## Stage 4: [BONUS] Unittest (**)
please implement 1 unit test per end point


## Stage 5: [BONUS] Deployment Yaml (**)
Modify the Deployment Yaml file to deploy the App to k8s, validate it is running in the cluster

