import pytest
from app.main import app as flask_app

@pytest.fixture
def app():
    yield flask_app

@pytest.fixture
def client(app):
    return app.test_client()

def test_index_positive(app, client):
    res = client.get('/')
    assert res.status_code == 200

def test_index_negative(app, client):
    res = client.get('/home')
    assert res.status_code == 404

def test_dummy():
    assert True